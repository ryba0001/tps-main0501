using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poursuite : MonoBehaviour{
    public Transform player;
    public float speed = 5f; // Vitesse de poursuite
    public float stopDistance = 1f; // Distance minimale pour s'arrêter
    private float initialY; // Position verticale initiale de l'objet poursuivant

    void Start()
    {
        if (player == null)
        {
            GameObject playerObject = GameObject.FindWithTag("Player");
            if (playerObject != null)
            {
                player = playerObject.transform;
            }
        }
        initialY = transform.position.y; // Stocker la position verticale initiale
    }

    // Update est appelé une fois par frame
    void Update()
    {
        if (player != null)
        {
            // Calculer la direction vers la cible
            Vector3 direction = player.position - transform.position;
            float distance = direction.magnitude;

            // Vérifier si la distance est inférieure à la distance minimale d'arrêt
            if (distance > stopDistance)
            {
                direction.Normalize();
                // Déplacer l'objet vers la cible sans changer sa rotation
                transform.position += direction * speed * Time.deltaTime;
            }

            // Ajuster la position verticale pour correspondre à celle du joueur
            transform.position = new Vector3(transform.position.x, player.position.y, transform.position.z);

            // Appliquer la rotation pour regarder vers la cible
            transform.rotation = Quaternion.LookRotation(direction);

            // Vérifier si l'objet poursuivant est au sol malgre le fait que le poursuivi tombe
            if (transform.position.y < initialY)
            {
                transform.position = new Vector3(transform.position.x, initialY, transform.position.z);
            }
        }
    }
}